# Manual Control demo for AERSP 597, spring 2020

## Introduction

The course AERSP597 looks at the way humans interact with machines,
specifically vehicles, using an approach founded in Cognitive Systems
Engineering.

One of the basics for understanding the control of vehicle is the
theory of human manual control, as pioneered by McRuer.

The `controltask.py` is a simple python program demonstrating manual
control. A closed-loop control task is programmed with either a
pursuit display or a compensatory display. After a run the control
data is analysed and then the open-loop characteristics and pilot
control characteristics are plotted.

## Technical details

The simulation uses the widely available open-source programming
language Python.

You will need a number of software components to run the
simulation. The software should run on major platforms (Windows, Apple
Mac OSX, Linux), and packages are available on the conda ecosystem.


Steps to take for installation:

1. Install the Anaconda python distribution, download from
   the [anaconda](https://www.anaconda.com/download/) site and install. Use
   the Python 3 version, Python 2.7 will be obsoleted in January 2020.

2. When on Windows, open the Anaconda powershell prompt, and enter the
   following commands to get the installation updated. On Linux or Mac OSX,
   simply open a terminal, and then enter:

        conda update conda
        conda update --all


3. Now add `conda-forge`, an additional software channel

        conda config --add channels conda-forge
        conda update --all


4. We need a number of packages for the simulation.

        conda install control
        conda install slycot
        conda install sdl2
        conda install pysdl2
        conda install json5
        conda install cython
        conda install pyopengl


5. To start the control task, run

        python controltask.py

   select the appropriate condition. After the control task the file
   is saved with the date and time of the run. A raw data plot of the
   input, system output and target is generated, and two bode plots,
   one of the open loop and a second of the pilot control behavior.

## Screenshot

![Screenshot of the display](screenshot.png)


## To Do

* Add an interface for designing forcing functions and setting
  controlled system parameters

* Enable averaging of multiple runs for analysis

* Add calculation of bias and variance of the estimate

* Add a predictor law and a predictor symbol to the display

* Add preview

## Resources

[SDL2](https://www.libsdl.org/) Simple  DirectMedia Layer.

[pyopengl](https://pyopengl.sourceforge.net/) OpenGL bindings for
python.

[control](https://github.com/python-control/python-control) Control
systems theory module for python.

[slycot](https://github.com/python-control/slycot) Numeric algorithms
for control theory.
