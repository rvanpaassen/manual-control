#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 27 12:37:36 2019
Manual control demonstration program

@author: Rene van Paassen
"""

import numpy as np

class ForcingFunction:
    """
    Sum-of-sines forcing function generator
    """

    def __init__(self, T, cycles, weights, sigma, seed=0):
        """
        Create a new forcingfunction object

        Parameters
        ----------
        T : float
            Base period of the function.
        cycles : array of int
            Frequencies.
        weights : array of numeric
            Relative amplitude values of the function.
        sigma: numeric
            Standard deviation of the function amplitude
        seed : integer, optional
            seed value for the random phases. The default is 0.

        """

        # check
        if len(weights) != len(cycles):
            raise ValueError("argument sizes do not match")

        # create a random number generator
        rand = np.random.RandomState(seed)
        self.args = dict(T=T, cycles=cycles, weights=weights, sigma=sigma,
                         seed=seed)
        # array of frequencies
        self.omega = 2 * np.pi * np.array(cycles) / T
        # initial phases
        self.phi0 = rand.uniform(0.0, 2.0*np.pi, len(cycles))
        # amplitudes
        self.amp =  np.array(weights) * sigma / \
            np.sqrt(0.5*np.sum(np.array(weights)**2))

    def value(self, t):
        """
        Calculate forcing function values

        Parameters
        ----------
        t : numeric
            Time.

        Returns
        -------
        Forcing function value.

        """
        try:
            return np.sum(np.sin(
                self.omega.reshape((-1,1)) @ t.reshape(1,-1) +
                self.phi0.reshape((-1,1))) * self.amp.reshape((-1,1)), axis=0)
        except:
            pass
        return np.sum(self.amp * np.sin(self.omega*t + self.phi0))

    def __str__(self):
        return "T={}, cycles={}, omega={}, seed={}, phi0={}, amplitude={}".format(
            self.args['T'], self.args['cycles'], self.omega.tolist(),
            self.args['seed'], self.phi0.tolist(), self.amp.tolist())

    def __repr__(self):
        return "ForcingFunction(T={T}, cycles={cycles}, "\
            "weights={weights}, sigma={sigma}, seed={seed})".format(
                **self.args)

# test one
if __name__ == '__main__':


    # small test for calculation
    t = np.arange(4)
    w = np.arange(1,3)
    p0 = np.arange(0.1,0.3,0.1)
    a = np.arange(10, 30, 10)
    print(w.reshape((-1,1)) @ t.reshape((1,-1)))
    print(w.reshape((-1,1)) @ t.reshape((1,-1)) + p0.reshape((-1,1)))
    print(np.sin(w.reshape((-1,1)) @ t.reshape((1,-1)) +
                 p0.reshape((-1,1))) * a.reshape(-1,1))


    T = 81.92
    cycles  = ( 2,  5,  7, 11, 17, 23, 31, 43, 53, 67, 83)
    weights = (10, 10, 10, 10,  1,  1,  1,  1,  1,  1,  1)
    ff = ForcingFunction(T, cycles, weights, 1.0, 0)

    tt = np.arange(0, 81.92, 0.01)
    u = ff.value(tt)

    from matplotlib import pyplot as plt

    plt.plot(tt, u)
    plt.show()

    print("var", np.var(u), str(ff), repr(ff))
