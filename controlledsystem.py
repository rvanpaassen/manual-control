#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 29 11:39:43 2019

@author: repa
"""

from control import matlab as ml
import numpy as np
from scipy import interpolate
from control import forced_response

class ControlledSystem:
    """
    Linear system dynamics, also stores the time trace
    """

    def __init__(self, num, den):
        """
        create a linear controlled system

        Parameters
        ----------
        num : array of numeric
            Numerator of the controlled system
        den : array of numeric
            Denominator of controlled system

        Returns
        -------
        Newly created system

        """
        self.tf = ml.TransferFunction(num, den)
        self.ss = ml.ss(self.tf)
        self.order = len(self.tf.pole())
        self.x = np.zeros((self.order,), dtype=float)
        self.dt = [0]
        self.u = [0]
        self.y = [0]
        self.lasttime = 0

    def step(self, dt, u):
        """
        Calculate the new state of the system

        Parameters
        ----------
        dt : numeric
            Time step in seconds.
        u : numeric
            Input value.

        Returns
        -------
        float
            output value.

        """
        resp = forced_response(self.tf, (0, dt), u, self.x, return_x=True)
        self.lasttime += dt
        self.x = resp.states[:,-1]
        self.u.append(u)
        self.dt.append(dt)
        self.y.append(resp.outputs[-1])
        return self.y[-1]

    def recording(self, dt):
        """
        Calculate input, output and time vector

        Parameters
        ----------
        dt : numeric
            Time step for re-sampling.

        Returns
        -------
        tuple of arrays, t, u, y.

        """
        t = np.cumsum(self.dt)
        fu = interpolate.interp1d(t, self.u)
        fy = interpolate.interp1d(t, self.y)

        # time vector
        t = np.arange(0, t[-1], dt)
        return t, fu(t), fy(t)

    def getT(self):
        """
        Get latest stored time

        Returns
        -------
        time

        """
        return self.lasttime

    def __str__(self):
        return "TransferFunction({},{})".format(self.tf.num[0][0].tolist(),
                                                self.tf.den[0][0].tolist())

    def __repr__(self):
        return "ControlledSystem({},{})".format(self.tf.num[0][0].tolist(),
                                                self.tf.den[0][0].tolist())

if __name__ == '__main__':

    # testing section
    sys = ControlledSystem([1], [1,0])

    for i in range(100):
        sys.step(0.03, 1)
    for i in range(30):
        sys.step(0.1, -1)

    from matplotlib import pyplot as plt

    t, u, y = sys.recording(0.05)
    fig = plt.figure()
    plt.plot(t,u, t, y)
    fig.suptitle(str(sys))
    print(str(sys))

    sys = ControlledSystem([1], [1,0,0])
    for i in range(100):
        sys.step(0.03, 1)
    for i in range(30):
        sys.step(0.1, 0)

    t, u, y = sys.recording(0.05)
    fig = plt.figure()
    fig.suptitle(str(sys))
    plt.plot(t,u, t, y)
    print(sys.ss.A)
    print(str(sys))
