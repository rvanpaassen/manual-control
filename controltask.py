#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 29 15:10:09 2019

@author: repa
"""

from forcingfunction import ForcingFunction
from display import TrackingDisplay
from controlledsystem import ControlledSystem
import matplotlib.pyplot as plt
import json
from datetime import datetime
from scipy.fftpack import fft
from control import matlab as ml
import glob
import numpy as np
import sys

class ControlTask:
    """
    Base class for compensatory and tracking tasks
    """
    def __init__(self, sys, ff):
        """
        Create a control task

        Parameters
        ----------
        sys : ControlledSystem
            System controlled in tracking task.
        ff : TYPE
            Forcing function.

        Returns
        -------
        None.

        """
        self.ff = ff
        self.sys = sys

    def getT(self):
        """
        Get the current time in the task

        Returns
        -------
        numeric
            Time in task.

        """
        return self.sys.getT()


class Compensatory(ControlTask):
    def __call__(self, t, dt, u):
        """
        calculate and return target position and system output

        Parameters
        ----------
        t : numeric
            Time in task, for forcing function calculation.
        dt : numeric
            Time step for system update.
        u : numeric
            Input control signal.

        Returns
        -------
        tuple(float, float)
            Display target position (0.0) and target-system error.

        """
        return 0.0, self.ff.value(t) - self.sys.step(dt, -u)

    def __str__(self):
        return "Compensatory task on system {}, with target {}".format(
            str(self.sys), str(self.ff))
    def __repr__(self):
        return "Compensatory(sys={}, ff={})".format(repr(self.sys),
                                                    repr(self.ff))


class Target(ControlTask):
    def __call__(self, t, dt, u):
        """
        calculate and return target position and system output

        Parameters
        ----------
        t : numeric
            Time in task, for forcing function calculation.
        dt : numeric
            Time step for system update.
        u : numeric
            Input control signal.

        Returns
        -------
        tuple(float, float)
            Display target position and system output.

        """
        return self.ff.value(t), self.sys.step(dt, u)
    def __str__(self):
        return "Target task on system {}, with target {}".format(
            str(self.sys), str(self.ff))
    def __repr__(self):
        return "Target(sys={}, ff={})".format(repr(self.sys),
                                                repr(self.ff))

if __name__ == '__main__':

    #%% for now, a number of standard tasks
    opt = 0
    while opt < 1 or opt > 10:
        print("""
              1: single integrator, compensatory
              2: single integrator, target
              3: single integrator, target, no forcing function
              4: double integrator, compensatory
              5: double integrator, target
              6: double integrator, target, no forcing function
              7: gain, compensatory
              8: gain, target
              9: gain, target, no forcing function
             10: load and analyze recorded file(s)""")

        try:
            opt = int(input("which task? "))
        except ValueError:
            pass

    #%% measure or read
    dt = 0.01
    T = 81.92
    if opt < 10:
        # create the controlled system
        if opt <= 3:
            sys = ControlledSystem([4], [1, 0])
        elif opt <= 6:
            sys = ControlledSystem([2], [1, 0, 0])
        elif opt <= 9:
            sys = ControlledSystem([2], [1])

        # create the forcing function
        Tmeas = 85
        cycles  = ( 2,  5,  7, 11, 17, 23, 31, 43, 53, 67, 83)
        weights = (10, 10, 10, 10,  1,  1,  1,  1,  1,  1,  1)

        if opt in (3, 6, 9):
            ff = ForcingFunction(T, cycles, weights, 0.0, 0)
        else:
            ff = ForcingFunction(T, cycles, weights, 0.4, 0)

        # create the task
        if opt in (2, 3, 5, 6, 8, 9):
            task = Target(sys, ff)
        else:
            task = Compensatory(sys, ff)

        # create a display
        mc = TrackingDisplay()

        print("For mouse control, task starts when you click in the display")

        # run the task
        while mc.cycle(task) and task.getT() < Tmeas:
            pass

        # close the display
        del mc

        # get the data
        t, u, y = sys.recording(dt)

        # save for the future
        filename = datetime.now().strftime("record-%m%d-%H%M.json")
        json.dump(dict(T=t.tolist(), U=u.tolist(), Y=y.tolist(),
                       description=repr(task)),
                  open(filename, 'w'))
        print(f"The measured data were saved in file {filename}")

    else:
        cfiles = glob.glob('record-????-????.json')
        cfiles.sort()
        for i,f in enumerate(cfiles):
            print("{}: {}".format(i, f))
        sel = []
        while not sel or max(sel) >= len(cfiles) or min(sel) < 0:
            try:
                print("""
Select the files you want to analyze, for estimating on the average of
multiple files, enter their numbers separated with comma's""")
                sel = [ int(i) for i in
                        input("Which file or files? ").split(',')]
            except ValueError:
                pass

        t = u = y = 0
        tdescription = None
        print(sel)
        for s in sel:
            res = json.load(open(cfiles[s], 'r'))
            if tdescription:
                if res['description'] != tdescription:
                    print("You selected files from different task conditions")
                    print("These cannot be combined for analysis")
                    print(tdescription)
                    print(res['description'])
                    sys.exit(0)
            else:
                tdescription = res['description']
                task = eval(res['description'])
                ff = task.ff
                cycles = ff.args['cycles']

            t = np.array(res['T']) + t
            u = np.array(res['U']) + u
            y = np.array(res['Y']) + y
        t = t / len(sel)
        u = u / len(sel)
        y = y / len(sel)

    #%% plot the raw results
    fig = plt.figure()
    plt.plot(t, u, t, y, t, ff.value(t))
    fig.suptitle("input signal, output signal, forcing function")

    #%% analyse control
    npts = int(round(T/dt))
    U = fft(u[-npts:])
    ft = ff.value(t)
    FF = fft(ft[-npts:])
    e = ft - y
    E = fft(e[-npts:])
    Hp = ml.frd(U[cycles,]/E[cycles,], ff.omega)
    Y = fft(y[-npts:])
    Ho = ml.frd(Y[cycles,]/E[cycles,], ff.omega)

    #%% print performance values
    evar = np.var(e[-npts:])
    uvar = np.var(u[-npts:])
    print(f"""
    Error variance (performance) {evar:6.4f}
    Control variance (effort)    {uvar:6.4f}""")

    # circular frequency
    omega = np.arange(0, 50, 1/T*2*np.pi)

    fig = plt.figure()
    plt.subplot(311)
    plt.step(omega, np.abs(FF[:len(omega)]), where='mid')
    ax = plt.gca(); ax.set_xscale('log')
    plt.subplot(312)
    plt.step(omega, np.abs(E[:len(omega)]), where='mid')
    ax = plt.gca(); ax.set_xscale('log')
    plt.subplot(313)
    plt.step(omega, np.abs(U[:len(omega)]), where='mid')
    ax = plt.gca(); ax.set_xscale('log')
    fig.suptitle("fft amplitude of test, error and control signals")

    fig = plt.figure()
    ml.bode(Hp, ff.omega, dB=False, Hz=False)
    fig.suptitle("Human operator transfer function")

    fig = plt.figure()
    ml.bode(Ho, ff.omega, dB=False, Hz=False)
    fig.suptitle("Combined open loop transfer function")

    plt.show()
