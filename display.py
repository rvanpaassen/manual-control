#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 27 14:27:53 2019

@author: repa
"""
import sdl2
import ctypes
from OpenGL import GL as gl
import numpy as np
from OpenGL.GL import shaders

# https://gist.github.com/hurricanerix/3be8221128d943ae2827

# vertex shader
VERT_SOURCE = """
#version 330
uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 proj_matrix;
in vec4 mc_vertex;
void main()
{
  mat4 mv_matrix = view_matrix * model_matrix;
  vec4 cc_vertex = mv_matrix * mc_vertex;
  gl_Position = proj_matrix * cc_vertex;
}"""

FRAG_SOURCE = """
#version 330
// in int mc_index;
out vec4 frag_color;
void main()
{
    frag_color = vec4(0.0, 1.0, 0.0, 1.0);
}"""

VERTICES = [ (np.sin(phi), np.cos(phi), 0, 1)
             for phi in np.arange(0, 2*np.pi+0.001, 0.2*np.pi) ]
VERTICES.extend([ (0, 1.0, 0, 1),
              (0, 0.5, 0, 1),
              (0, -0.5, 0, 1),
              (0, -1, 0, 1)])
VERTICES = np.array(VERTICES, dtype=np.float32).reshape((-1,))


def get_4x4_transform(scale_x, scale_y, trans_x, trans_y, trans_z):
    """Transform the from local coordinates to world coordinates.
    @return: transformation matrix used to transform from local coords
             to world coords.
    @rtype: Matrix44
    """
    """transform = Matrix44()
    transform.set_row(0, [scale_x, 0.0, 0.0, trans_x])
    transform.set_row(1, [0.0, scale_y, 0.0, trans_y])
    transform.set_row(2, [0.0, 0.0, 1.0, trans_z])
    transform.set_row(3, [0.0, 0.0, 0.0, 1.0])
    return transform.to_opengl()"""
    return np.array( ( ( scale_x, 0.0, 0.0, trans_x ),
    (0.0, scale_y, 0.0, trans_y),
    (0.0, 0.0, 1.0, trans_z),
    (0.0, 0.0, 0.0, 1.0)), np.float32)

def _get_projection_matrix(left, right, bottom, top):
    """Create a  orthographic projection matrix.
    U{Modern glOrtho2d<http://stackoverflow.com/questions/21323743/
    modern-equivalent-of-gluortho2d>}
    U{Orthographic Projection<http://en.wikipedia.org/wiki/
    Orthographic_projection_(geometry)>}
    @param left: position of the left side of the display
    @type left: int
    @param right: position of the right side of the display
    @type right: int
    @param bottom: position of the bottom side of the display
    @type bottom: int
    @param top: position of the top side of the display
    @type top: int
    @return: orthographic projection matrix
    @rtype: Matrix44
    """
    zNear = -25.0
    zFar = 25.0
    inv_z = 1.0 / (zFar - zNear)
    inv_y = 1.0 / (top - bottom)
    inv_x = 1.0 / (right - left)
    mat = np.array(
        ([(2.0 * inv_x), 0.0, 0.0, (-(right + left) * inv_x)],
         [0.0, (2.0 * inv_y), 0.0, (-(top + bottom) * inv_y)],
         [0.0, 0.0, (-2.0 * inv_z), (-(zFar + zNear) * inv_z)],
         [0.0, 0.0, 0.0, 1.0]), np.float32)
    return mat

def _get_view_matrix(x, y):
    scale_x = 1.0
    scale_y = 1.0
    trans_x = x
    trans_y = y
    layer = 1.0
    return get_4x4_transform(scale_x, scale_y, trans_x, trans_y, layer)

WINDOW_WIDTH = 800
WINDOW_HEIGHT = 600

class TrackingDisplay:
    """
    Present a compensatory or pursuit display
    """
    def __init__(self):
        """
        Create a display for tracking, compensatory or target

        Raises
        ------
        AssertionError
            Can raise errors for GL problems.

        Returns
        -------
        None.

        """

        self.running = False
        self.u = 0
        self.lasttime = 0
        self.elapsed = 0.0

        # Init
        sdl2.SDL_Init(sdl2.SDL_INIT_EVERYTHING)
        sdl2.SDL_GL_SetAttribute(sdl2.SDL_GL_CONTEXT_MAJOR_VERSION, 3)
        sdl2.SDL_GL_SetAttribute(sdl2.SDL_GL_CONTEXT_MINOR_VERSION, 2)
        sdl2.SDL_GL_SetAttribute(sdl2.SDL_GL_CONTEXT_PROFILE_MASK,
                             sdl2.SDL_GL_CONTEXT_PROFILE_CORE)
        sdl2.SDL_GL_SetAttribute(sdl2.SDL_GL_DOUBLEBUFFER, 1)
        sdl2.SDL_GL_SetAttribute(sdl2.SDL_GL_DEPTH_SIZE, 24)
        sdl2.SDL_GL_SetSwapInterval(1)
        joystick = sdl2.SDL_JoystickOpen(0)
        if joystick:
            print("Joystick detected, press button to start task")

        self.window = sdl2.SDL_CreateWindow(
            "manual control".encode('utf-8'), sdl2.SDL_WINDOWPOS_CENTERED,
            sdl2.SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT,
            sdl2.SDL_WINDOW_OPENGL | sdl2.SDL_WINDOW_SHOWN)
        if not self.window:
            raise AssertionError("cannot make window")
        glcontext = sdl2.SDL_GL_CreateContext(self.window)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glEnable(gl.GL_CULL_FACE)
        gl.glEnable(gl.GL_BLEND)
        gl.glClearColor(0.3, 0.3, 0.3, 1.0)

        self.attrib_locs = {
            "mc_vertex": -1,
        }
        self.uniform_locs = {
            "model_matrix": -1,
            "view_matrix":  -1,
            "proj_matrix":  -1,
        }
        vert_prog = shaders.compileShader(VERT_SOURCE, gl.GL_VERTEX_SHADER)
        if not gl.glGetShaderiv(vert_prog, gl.GL_COMPILE_STATUS):
            raise AssertionError("Could not compile vertex shader")
        frag_prog = shaders.compileShader(FRAG_SOURCE, gl.GL_FRAGMENT_SHADER)
        if not gl.glGetShaderiv(frag_prog, gl.GL_COMPILE_STATUS):
            raise AssertionError("Could not compile fragment shader")
        self.shader = gl.glCreateProgram()
        gl.glAttachShader(self.shader, vert_prog)
        gl.glAttachShader(self.shader, frag_prog)
        gl.glLinkProgram(self.shader)
        if gl.glGetProgramiv(self.shader, gl.GL_LINK_STATUS) != gl.GL_TRUE:
            raise AssertionError(gl.glGetProgramInfoLog(self.shader))

        for name in self.attrib_locs:
            self.attrib_locs[name] = gl.glGetAttribLocation(self.shader, name)
        for name in self.uniform_locs:
            self.uniform_locs[name] = gl.glGetUniformLocation(self.shader, name)

        # Load Object
        self.vao = gl.glGenVertexArrays(1)
        gl.glBindVertexArray(self.vao)
        vertex_buffer = gl.glGenBuffers(1)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, vertex_buffer)
        gl.glBufferData(gl.GL_ARRAY_BUFFER, len(VERTICES) * 4, VERTICES,
                        gl.GL_STATIC_DRAW)
        gl.glVertexAttribPointer(self.attrib_locs['mc_vertex'], 4,
                                 gl.GL_FLOAT, False,
                                 0, ctypes.c_void_p(0))


        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, 0)
        gl.glBindVertexArray(0)

    def __del__(self):
        """
        Delete tracking window

        Returns
        -------
        None.

        """

        gl.glUseProgram(0)
        sdl2.SDL_DestroyWindow(self.window)
        sdl2.SDL_Quit()

    def cycle(self, sim):
        """
        Perform a drawing and calculation cycle

        Parameters
        ----------
        sim : function
            Calculates simulation step and returns target and output.

        Returns
        -------
        bool
            If True, continue calling for next cycle.

        """

        event = sdl2.SDL_Event()

        while sdl2.SDL_PollEvent(ctypes.byref(event)) != 0:
            if event.type == sdl2.SDL_QUIT:
                return False
            if event.type == sdl2.events.SDL_KEYDOWN:
                print("SDL_KEYDOWN")
                if event.key.keysym.sym == sdl2.SDLK_ESCAPE:
                    return False
            if event.type == sdl2.SDL_MOUSEMOTION and self.running == 1:
                self.u = event.motion.x / WINDOW_WIDTH * 2 - 1
                #print(self.u)
            if event.type == sdl2.SDL_JOYAXISMOTION and self.running == 2:
                if event.jaxis.axis == 0:
                    self.u = event.jaxis.value / 2**16
            if event.type == sdl2.SDL_MOUSEBUTTONDOWN:
                if self.running:
                    self.running = False
                else:
                    self.running = 1
            if event.type == sdl2.SDL_JOYBUTTONUP:
                if self.running:
                    self.running = False
                else:
                    self.running = 2


        timenow = sdl2.SDL_GetTicks()
        dt = 0.001 * (timenow - self.lasttime)
        y = 0
        target = 0

        self.lasttime = timenow
        if self.running:
            self.elapsed += dt
            target, y = sim(self.elapsed, dt, self.u)
            self.object_x = y * 0.5 * WINDOW_WIDTH

        # Update model_matrix
        object_w = 10
        object_h = 10
        object_x = WINDOW_WIDTH / 2 + y * 0.5 * WINDOW_WIDTH
        object_y = WINDOW_HEIGHT / 2
        model_matrix = get_4x4_transform(scale_x=object_w, scale_y=object_h,
                                         trans_x=object_x, trans_y=object_y,
                                         trans_z=1.0)

        # update target matrix
        object_h = 30
        object_x = WINDOW_WIDTH / 2 + target * 0.5 * WINDOW_WIDTH
        target_matrix = get_4x4_transform(scale_x=object_w, scale_y=object_h,
                                         trans_x=object_x, trans_y=object_y,
                                         trans_z=1.0)

        # Update proj_matrix
        proj_matrix = _get_projection_matrix(0.0, WINDOW_WIDTH,
                                             0.0, WINDOW_HEIGHT)
        # Update view_matrix
        view_matrix = _get_view_matrix(1.0, 1.0)

        # Start Rendering
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        gl.glUseProgram(self.shader)

        # Draw object
        gl.glBindVertexArray(self.vao)
        gl.glEnableVertexAttribArray(self.attrib_locs['mc_vertex'])
        gl.glUniformMatrix4fv(self.uniform_locs['model_matrix'], 1, gl.GL_TRUE,
                              model_matrix)
        gl.glUniformMatrix4fv(self.uniform_locs['view_matrix'], 1, gl.GL_TRUE,
                              view_matrix)
        gl.glUniformMatrix4fv(self.uniform_locs['proj_matrix'], 1, gl.GL_TRUE,
                              proj_matrix)
        gl.glDrawArrays(gl.GL_LINE_LOOP, 0, len(VERTICES) // 4 - 4)


        gl.glUniformMatrix4fv(self.uniform_locs['model_matrix'], 1, gl.GL_TRUE,
                              target_matrix)
        gl.glDrawArrays(gl.GL_LINES, len(VERTICES) // 4 - 4, len(VERTICES) // 4)

        # Stop Rendering
        gl.glBindVertexArray(0)
        gl.glUseProgram(0)
        sdl2.SDL_GL_SwapWindow(self.window)
        return True

if __name__ == '__main__':
    mc = TrackingDisplay()

    from forcingfunction import ForcingFunction
    from controlledsystem import ControlledSystem
    from matplotlib import pyplot as plt
    from controltask import Target
    import json

    sys = ControlledSystem([4], [1, 0])

    Tmeas = 10
    Tff = 81.92


    ff = ForcingFunction(81.92, [2, 5, 11], [1, 1, 1], 0.5)

    task = Target(sys, ff)

    while mc.cycle(task) and task.getT() < Tmeas:
        print(sys.getT())
        #.print("cycle")
        pass

    del mc

    t, u, y = sys.recording(0.1)
    plt.plot(t, u, t, y, t, ff.value(t))
    plt.show()

    print(json.dumps(dict(Tmeas=Tmeas, Tff=Tff, ff=ff.value(t).tolist(),
                          T=t.tolist(), U=u.tolist(), Y=y.tolist(),
                          description=str(task)) ))
